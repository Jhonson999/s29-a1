
const express = require('express')

const app = express();


const port = 4000;


app.use(express.json())

app.use(express.urlencoded({extended: true}));


let users = [
     {
     	email:"nezukoKamado@gmail.com",
     	username: "nezuko01",
     	password: "letMeOut",
     	isAdmin: false
     },
     {
     	email:"nezukoKamado@gmail.com",
     	username: "gonpanchiro",
     	password: "iAmTanjiro",
     	isAdmin: false
     },
     {
     	email:"zenitsuAgatsuma@gmail.com",
     	username: "zenitsuSleeps",
     	password: "iNeedNezuko",
     	isAdmin: true
     },

]


// 1. Create a GET route that will access the /home route that will print out a simple message.

app.get('/home', (req, res) => {
	res.send('This is your homepage.')
});

// 2. Create a GET route that will access the /users route that will retrieve all the users in the mock database.

app.get('/users', (req, res) => {
	res.send(users)
});


app.delete('/delete-user', (req, res) => {
  
     let message;
     
    
     for(let i = 0; i < users.length; i++){
         
         
          if(req.body.username === users[i].username){
              
             
               users[i].password = req.body.password;
               
              
               message = `User ${req.body.username} has been deleted.`;

              
               break;
          
          } else {
               // changes the message to be sent back as a response
               message = `User not found.`
          }
     }

     // response that will be sent to our client
     res.send(message)
})


app.listen(port, () => console.log(`The Server is running at port ${port}`));

